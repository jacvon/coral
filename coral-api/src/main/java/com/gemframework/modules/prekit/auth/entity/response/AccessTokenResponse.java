package com.gemframework.modules.prekit.auth.entity.response;

import com.gemframework.model.entity.po.Member;
import com.gemframework.modules.prekit.auth.entity.request.AuthRequest;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class AccessTokenResponse extends AuthRequest {

    private String accessToken;
    private String refreshToken;
    private String expiresIn;
    private Member member;

}
